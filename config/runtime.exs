import Config

config :libcluster,
  topologies: [
    kubernetes: [
      # The selected clustering strategy. Required.
      strategy: Cluster.Strategy.Kubernetes,
      config: [
        mode: :dns,
        kubernetes_ip_lookup_mode: :pods,
        kubernetes_node_basename: "cluster_exp",
        kubernetes_selector: "app=cluster-exp",
        kubernetes_namespace: System.fetch_env!("NAMESPACE"),
        polling_interval: 10_000
      ]
    ]
]
