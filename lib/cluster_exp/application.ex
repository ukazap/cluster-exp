defmodule ClusterExp.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    topologies = Application.get_env(:libcluster, :topologies)
    children = [
      # Starts a worker by calling: ClusterExp.Worker.start_link(arg)
      # {ClusterExp.Worker, arg}
      {Cluster.Supervisor, [topologies, [name: ClusterExp.ClusterSupervisor]]},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ClusterExp.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
