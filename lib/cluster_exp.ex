defmodule ClusterExp do
  @moduledoc """
  Documentation for `ClusterExp`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> ClusterExp.hello()
      :world

  """
  def hello do
    :world
  end
end
