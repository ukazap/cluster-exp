import Config

if config_env() == :prod do
  config :libcluster,
    topologies: [
      kubernetes: [
        # The selected clustering strategy. Required.
        strategy: Cluster.Strategy.Kubernetes,
        config: [
          mode: :dns,
          kubernetes_node_basename: "cluster_exp",
          kubernetes_selector: "app=cluster_exp",
          kubernetes_namespace: "mynamespace",
          polling_interval: 10_000
        ]
      ]
  ]
else
  config :libcluster,
    topologies: [
      example: [
        # The selected clustering strategy. Required.
        strategy: Cluster.Strategy.Gossip,
        config: [
          secret: "cluster_exp"
        ]
      ]
  ]
end
