defmodule ConnectionCheck do
  def ping() do
    Node.list() |> Enum.map(fn node -> :rpc.call(node, ConnectionCheck, :pong, []) end )
  end

  def pong() do
    "PONG from #{Node.self}"
  end
end
