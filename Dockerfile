# FROM registry.gitlab.com/stockbitgroup/coinbit/base-images/elixir:1.14.0-no-jit AS app_builder
FROM --platform=linux/amd64 registry.gitlab.com/kadek.dermawan/base-images/elixir:1.14.0-no-jit AS app_builder

ARG APP_ENV=prod

ENV MIX_ENV=${APP_ENV} \
    LANG=C.UTF-8 \
    PATH=/root/.mix/escripts:$PATH \
    REPLACE_OS_VARS=true \
    TERM=xterm \
    HEX_HTTP_TIMEOUT=300

RUN apt-get update && \
    apt-get -y install autoconf build-essential libssl-dev curl

WORKDIR /app

# Install Hex + Rebar
RUN mix do local.hex --force, local.rebar --force

COPY . /app


RUN mix do deps.get, deps.compile
RUN mix release

# ---- Application Stage ----
FROM --platform=linux/amd64 debian:bullseye-slim
# FROM --platform=linux/amd64 python:3.8-slim-bullseye

RUN apt-get update && \
    apt-get -y install bash procps openssl iproute2 curl jq libsnappy-dev net-tools nano && \
    rm -rf /var/lib/apt/lists/*

ENV LANG=C.UTF-8 

WORKDIR /app
COPY --from=app_builder app/_build/prod/rel/cluster_exp .

EXPOSE 4369 
EXPOSE 9100-9109 

CMD ["./bin/cluster_exp", "start"]
